//
// src/src/anion-grammar.peg.rs
//
// Ion grammar specification file to be built by build.rs and
// loaded in src/parser.rs
//

//
// Ion Value - Rule parsing all types
//

pub ion -> AnionValue
  = t:timestamp_result { AnionValue::Timestamp(t) }
  / f:float_result { AnionValue::Float(f) }
  / d:decimal_result { AnionValue::Decimal(d) }
  / i:int_result { AnionValue::Integer(i) }
  / b:blob_result { AnionValue::Blob(b) }
  / l:list_result { AnionValue::List(l) }
  / s:struct_result { AnionValue::Struct(s) }
  / s:symbol_result { AnionValue::Symbol(s) }
  / s:string_result { AnionValue::Str(s) }
  / null_result { AnionValue::Null }


//
// Integer Parsing
//

int_result -> Option<AnionInt>
  = x:hex_int { Some(AnionInt::from_str_radix(&x, 16)) }
  / x:oct_int { Some(AnionInt::from_str_radix(&x, 8)) }
  / x:bin_int { Some(AnionInt::from_str_radix(&x, 2)) }
  / x:dec_int { Some(AnionInt::from_str_radix(&x, 10)) }
  / "null.int" { None }


dec_int -> String
  = n:$("-"? !"00" under_digits) { n.replace("_", "") }

hex_int -> String
  = n:$("-"? "0" [Xx] hex_digit+ hex_digits*) { reduce_int_string(n) }

oct_int -> String
  = n:$("-"? "0" [Oo] oct_digit+ oct_digits*) { reduce_int_string(n) }

bin_int -> String
  = n:$("-"? "0" [Bb] bin_digit+ bin_digits*) { reduce_int_string(n)  }

dec_digits = "_" dec_digit+ / dec_digit+
hex_digits = "_" hex_digit+ / hex_digit+
oct_digits = "_" oct_digit+ / oct_digit+
bin_digits = "_" bin_digit+ / bin_digit+

dec_digit = [0-9]
bin_digit = [0-1]
hex_digit = [0-9a-fA-F]
oct_digit = [0-7]

under_digits = dec_digit+ dec_digits*


//
// Decimal/Float Parsing
//

decimal_result -> Option<AnionDecimal>
  = x: reduced_decimal_string { Some(AnionDecimal::from(x)) }
  / "null.decimal" { None }

reduced_decimal_string -> String
  = d: decimal_string { d.replace("_", "").replace("d", "e").replace("D", "e") }

decimal_string -> String
  = d: $("-"? (num_with_decimal_point d_exponent? / under_digits d_exponent)) { d.into() }


float_result -> Option<AnionFloat>
  = f: float_val { Some(AnionFloat::from(f)) }
  / "null.float" { None }

float_val -> f64
  = num: reduced_float_string { num.parse().unwrap() }

reduced_float_string -> String
  = f: $(float_match) { f.replace("_", "") }

float_match = "-"? (num_with_decimal_point / under_digits) e_exponent


num_with_decimal_point
  = !"0" under_digits "." under_digits?
  / "0." under_digits?
  / "." under_digits

e_exponent = [eE] plus_or_minus? dec_digit+ dec_digits*
d_exponent = [dD] plus_or_minus? dec_digit+ dec_digits*

plus_or_minus = "+" / "-"


//
// Timestamp parsing
//

timestamp_result -> Option<AnionTimestamp>
  = ts: timestamp { Some(ts) }
  / "null.timestamp" { None }

timestamp -> AnionTimestamp
  = dt:$(ymdhmss) tz:$(timezone_str) { timestamp::from_ymdhmss(dt, tz) }
  / dt:$(ymdhms) tz:$(timezone_str)  { timestamp::from_ymdhms(dt, tz) }
  / dt:$(ymdhm) tz:$(timezone_str)   { timestamp::from_ymdhm(dt, tz) }
  / dt:$(ymdh) tz:$(timezone_str)    { timestamp::from_ymdh(dt, tz) }
  / dt:$(ymd) "T"?                   { timestamp::from_ymd(dt) }
  / dt:$(ym) "T"                     { timestamp::from_ym(dt) }
  / dt:$(y) "T"                      { timestamp::from_y(dt) }

y = year_num
ym = y "-" month_num
ymd = ym "-" day_num
ymdh = ymd "T" hour_num
ymdhm = ymdh ":" minute_num
ymdhms = ymdhm ":" second_num
ymdhmss = ymdhms "." [0-9]+

full_date = year_num "-" month_num "-" day_num

day_num = [0][1-9] / [1][0-9] / [2][0-9] / [3][0-1]
month_num = [0][1-9] / [1][0-2]
year_num = [0-9][0-9][0-9][0-9]

hour_num = [0-9][0-9]
minute_num = [0-9][0-9]
second_num = [0-9][0-9]


valid_year = [1-9][0-9]*<3>
           / [0] [1-9] [0-9]*<2>
           / [0] [0] [1-9] [0-9]
           / [0] [0] [0] [1-9]
valid_month = [0][1-9] / [1][0-3]
valid_day = [0][1-9] / [2][0-9] / [3][1-2]

valid_hour = [01][0-9] / [2][0-3]
valid_minute = [0-5][0-9]
valid_second = [0-5][0-9] / "60"

timezone_str = plus_or_minus [0-9][0-9] ":" [0-9][0-9] / "Z"


//
// String/Symbol Parsing
//

string_result -> Option<AnionString>
  = x: $(double_quote string_char* double_quote) { Some(trim_and_unescape_string(x).into()) }
  / "null.string" { None }

string_char = escape_seq / !"\"" !"\\" .

escape_seq
  = "\\\"" / "\\\\" / "\\'" / "\\/" / "\\?"
  / "\\a"/ "\\b" / "\\t" / "\\n" / "\\f" / "\\r" / "\\v"
  / "\\x" hex_digit*<2> / "\\u" hex_digit*<4> / "\\U" hex_digit*<8>
  / "\\NL" // nothing character - goes away...

double_quote = "\""

symbol_result -> Option<AnionSymbol>
  = "'" "'" { Some("".into()) }
  / x: $(symbol_quoted) { Some(trim_and_unescape_string(x).into()) }
  / x: $(symbol_identifier) { Some(x.into()) }
  / "null.symbol" { None }


// Quoted symbol: a sequence of zero or more characters between
// single-quotes, e.g., 'hello', 'a symbol', '123', ''. This
// representation can denote any symbol text.
symbol_quoted = "'" "\\" symbol_quoted
              / "'" [^']+ "'"

// Identifier: an unquoted sequence of one or more ASCII letters,
// digits, or the characters $ (dollar sign) or _ (underscore), not
// starting with a digit.
symbol_identifier = !("null" / [0-9]) symbol_char+
                  / "null" symbol_char+

symbol_char = [0-9a-zA-Z_$]

// Operator: an unquoted sequence of one or more of the following
// nineteen ASCII characters: !#%&*+-./;<=>?@^`|~ Operators can only
// be used as (direct) elements of an S-expression. In any other
// context those characters require single-quotes.
symbol_operator = [!#%&*+-./;<=>?@^`|~]


//
// Blob/Clob Parsing
//

blob_result -> Option<AnionBlob>
  = "{{" ws*  base64:$(base64_char* "="*) ws* "}}" { Some(base64.into()) }
  / "{{" ws * "}}" { Some(Vec::new().into()) }
  / "null.blob" { None }

base64_char = [a-zA-Z0-9+/]

//
// List Parsing
//

list_result -> Option<AnionList>
  = "[" ws* values: ion ** comma comma? ws* "]" { Some(values.into()) }
  / "null.list" { None }

// comma surrounded by whitespace
comma = ws* "," ws*
ws = [ \t]


//
// Struct Parsing
//

struct_result -> Option<AnionStruct>
  = "{" ws* "}" { Some(AnionStruct::new()) }
  / "{" ws* pairs: symbol_value_pair ** comma comma? ws* "}" {
    let mut obj = AnionStruct::new();
    for pair in pairs { obj.insert(pair.0, pair.1); }
    Some(obj)
  }
  / "null.struct" { None }


symbol_value_pair -> (AnionSymbol, AnionValue)
  = key:symbol_result ws* ":" ws* value:ion { (key.unwrap(), value) }

//
// null
//

null_result = "null.null" / "null"
