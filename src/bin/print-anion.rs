//! [\file]:# (src/bin/print-anion.rs)
//!
//! A simple executable for checking various text input at command line.
//!

extern crate anion;


fn main()
{
  for s in std::env::args().skip(1) {

    use anion::AnionValue::*;

    let val = anion::parse(s.as_str()).unwrap();

    match val {
      Boolean(Some(x)) => println!("Bool {}", x),
      Boolean(None) => println!("Bool NULL"),
      Integer(Some(x)) => println!("Int {}", x),
      Integer(None) => println!("Int NULL"),
      Float(Some(x)) => println!("Float {}", x),
      Float(None) => println!("Float NULL"),
      Decimal(Some(x)) => println!("Decimal {}", x),
      Decimal(None) => println!("Decimal NULL"),
      Str(Some(x)) => println!("String {}", x),
      Str(None) => println!("String NULL"),
      List(Some(x)) => println!("List {}", x),
      List(None) => println!("List NULL"),
      Symbol(Some(s)) => println!("Symbol '{}'", s),
      Symbol(None) => println!("Symbol NULL"),
      Timestamp(Some(s)) => println!("Timestamp {}", s),
      Timestamp(None) => println!("Timestamp NULL"),
      Blob(Some(s)) => println!("Blob {}", s),
      Blob(None) => println!("Blob NULL"),
      Struct(Some(s)) => println!("struct {}", s),
      Struct(None) => println!("struct NULL"),
      Null => println!("NULL"),
    }
  }
}
