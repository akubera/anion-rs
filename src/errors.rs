//! [\file]:# (src/errors.rs)
//!
//! All crate error types can be found here.
//!

use std::{error, io, fmt};

use parser::ParseError;
use types::errors::*;
// use types::int::ParsingError as IntError;

/// "Master" Error type enumerating all possible failures when dealing
/// with anion types.
///
pub enum Error {
  /// Generic parsing error
  ParseError(ParseError),

  /// Error while parsing an integer
  IntError(IntegerError),

  /// General parsing error
  UnexpectedCharacter(String, usize, usize),

  /// Raised when a null value is found where there shouldn't be (eg NonNullAnionValue)
  UnexpectedNull,

  /// IO-specific error
  Io(io::Error),
}

impl error::Error for Error {
  fn description(&self) -> &str
  {
    use Error::*;
    match *self {
      UnexpectedCharacter(..) => "syntax error",
      UnexpectedNull => "unexpected null",
      IntError(ref error) => error::Error::description(error),
      Io(ref error) => error::Error::description(error),
      ParseError(ref error) => error::Error::description(error),
    }
  }
}

impl fmt::Display for Error {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    use Error::*;
    match *self {
      //       ParseError(ref err) => write!(f, "IO error: {}", err),
      UnexpectedCharacter(ref code, line, col) => {
        if line == 0 && col == 0 {
          write!(f, "{}", code)
        } else {
          write!(f, "{} at {}:{}", code, line, col)
        }
      },
      UnexpectedNull => f.write_str("Unexpected null value"),
      Io(ref err) => fmt::Display::fmt(err, f),
      ParseError(ref err) => fmt::Display::fmt(err, f),
      IntError(ref err) => fmt::Display::fmt(err, f),
    }
  }
}

impl fmt::Debug for Error {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    use Error::*;
    match *self {
      IntError(ref err) => fmt::Debug::fmt(err, f),
      ParseError(ref err) => fmt::Debug::fmt(err, f),
      UnexpectedCharacter(ref code, ref line, ref col) => {
        f.debug_tuple("Syntax")
          .field(code)
          .field(line)
          .field(col)
          .finish()
      },
      Io(ref io) => {
        f.debug_tuple("IO")
          .field(io)
          .finish()
      },
      UnexpectedNull => f.debug_tuple("null").finish(),
    }
  }
}
