//! [\file]:# (src/lib.rs)
//!
//! Rust implementation of Amazon's [Ion data format][ION_DOCS]
//!
//! Ion is a data format very similar to JSON, with enhancements such
//! as additional types (timestamps, s-expressions, etc), typed
//! null values (an unset integer would be `null.int`), and ability
//! to add annotations to all values.
//!
//! This implementation is *very* alpha and subject to massive changes.
//! Help and suggestions in its development would be appreciated.
//!
//! Learn more by checking out the [specification][ION_SPEC] and the
//! [java implementation][JAVA_IMP].
//!
//!
//! [ION_DOCS]: https://amznlabs.github.io/ion-docs/
//! [ION_SPEC]: https://amznlabs.github.io/ion-docs/spec.html
//! [JAVA_IMP]: https://github.com/amznlabs/ion-java
//!

#![allow(unreachable_code)]

extern crate base64;
extern crate num_traits;
extern crate num_bigint;
extern crate bigdecimal;

#[macro_use]
extern crate cfg_if;

#[cfg(feature = "chrono")]
extern crate chrono;
#[cfg(feature = "serde")]
extern crate serde;


#[macro_use]
mod macros;
mod errors;
mod value;
mod types;

pub mod parser;

#[cfg(feature = "serde")]
pub mod ser_de;

pub use value::{AnionValue, NonNullAnionValue};

#[allow(unused_attributes)]
#[cfg_attr(rustfmt, rustfmt_skip)]
pub use types::{
  AnionBool,
  AnionFloat,
  AnionInt,
  AnionDecimal,
  AnionString,
  AnionSymbol,
  AnionList,
  AnionStruct,
  AnionTimestamp,
  AnionBlob,
};

/// Result wrapping all possible errors in this crate
pub type Result<T> = std::result::Result<T, Error>;
pub use errors::Error;


/// Return equivalent AnionValue from some object.
/// Convenience for `AnionValue::from(..)`.
///
/// **Note:** This does not *parse* the value, but returns an equivalent
/// type, so anion::from("42") is an AnionString, while anion::from(42)
/// is an AnionInt.
///
/// This function is limited by the rust type system; there is no
/// equivalent s-expression or symbol types in rust, so those will
/// need to be explicity cast from a string.
///
/// ### Example Usage
///
/// ```rust
/// use anion;
/// let val = anion::from("0xf2");
/// assert_eq!(val, anion::AnionValue::Str(Some(anion::AnionString::from("0xf2"))));
/// assert_eq!(anion::from(42), anion::AnionInt::from(42));
/// ```
///
#[inline]
pub fn from<T>(value: T) -> AnionValue
  where T: Into<AnionValue>
{
  value.into()
}

/// Parse a string and try to create an AnionValue.
/// Convenience method for `str::parse::<AnionValue>(..)`.
///
/// **Note** Unlike `anion::from` this interprets a string as a
/// value, so anion::parse("42") returns an integer type, while
/// anion::parse("\"42\"") returns a string-type.
///
/// ### Example Usage
///
/// ```rust
/// use anion;
/// let val = anion::parse("1234").unwrap();
/// assert_eq!(val, anion::AnionValue::Integer(Some(anion::AnionInt::from(1234))));
/// # //assert_eq!(val, 1234);
/// assert_eq!(val, anion::parse("0x4d2").unwrap());
/// ```
///
#[inline]
pub fn parse(source: &str) -> Result<AnionValue>
{
  source.parse()
}
