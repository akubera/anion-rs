//! [\file]:# (src/macros.rs)
//!
//! Macros for automatically building Ion values
//!


/// Macro to easily build an <a class="type" href="type.AnionList.html">AnionList</a>; similiar to `vec![]`
///
/// ```rust
/// #[macro_use]
/// extern crate anion;
/// # use anion::{AnionInt, AnionString, AnionList};
///
/// # fn main() {
/// let data = ion_list![1, "String"];
///
/// assert_eq!(data[0], AnionInt::from(1));
/// assert_eq!(data[1], AnionString::from("String"));
/// # }
/// ```
#[macro_export]
macro_rules! ion_list {
  [] => ($crate::AnionList::new());

  [ $( $item:expr ),* ] => ({
      let mut list = $crate::AnionList::new();
      $( list.push($item.into()); )*
      list
  })
}


/// Macro to easily build an <a class="type" href="type.AnionStruct.html">AnionStruct</a>
///
/// ```rust
/// #[macro_use]
/// extern crate anion;
/// # use anion::{AnionValue, AnionInt};
///
/// # fn main() {
/// let o = ion_struct!{
///   a: "abc", num: 42
/// };
///
/// assert_eq!(o.get(&"num".into()).unwrap(), &AnionValue::from(42));
/// assert_eq!(o.get(&"a".into()).unwrap(), &AnionValue::from("abc"));
/// # }
/// ```
///
#[macro_export]
macro_rules! ion_struct {

  { $key:ident : { $($val:tt)* } , $($rest:tt)* } => {{
    let mut obj = ion_struct!{};
    ion_struct_extend!(obj; $key -> ion_struct!{ $($val)* } , $($rest)* );
    obj
  }};

  { $key:ident : [ $($val:tt)* ] , $($rest:tt)* } => {{
    let mut obj = ion_struct!{};
    ion_struct_extend!(obj; $key -> ion_list![ $($val)* ] , $($rest)* );
    obj
  }};

  { $key:ident : $val:expr , $($rest:tt)* } => {{
    let mut obj = ion_struct!{};
    ion_struct_extend!(obj; $key : $val, $($rest)* );
    obj
  }};


  { $key:ident : { $($val:tt)* } } => {{
    ion_struct!($key : { $($val)* } ,)
  }};

  { $key:ident : [ $($val:tt)* ] } => {{
    ion_struct!($key : [ $($val)* ] ,)
  }};

  { $key:ident : $val:expr } => {{
    ion_struct!($key : $val ,)
  }};

  {} => {
    $crate::AnionStruct::new()
  };
}

/// Macro to add elements to a (mutable) <a class="type" href="type.AnionStruct.html">AnionStruct</a> object
///
/// The macro follows a pattern `<ident>; ...` where ident is the object name to modify
/// and `...` is the rest of the pattern used to construct and insert values.
///
/// This macro is used by `ion_struct!` to extend the objects created there.
///
/// ```rust
/// #[macro_use]
/// extern crate anion;
/// # use anion::{AnionValue};
///
/// # fn main() {
/// let mut o = ion_struct!{
///   a: "abc", num: 42
/// };
///
/// ion_struct_extend!{o;
///   c: 123
/// };
///
/// assert_eq!(o.get(&"c".into()).unwrap(), &AnionValue::from(123));
/// assert_eq!(o.get(&"num".into()).unwrap(), &AnionValue::from(42));
/// assert_eq!(o.get(&"a".into()).unwrap(), &AnionValue::from("abc"));
/// # }
/// ```
///
#[macro_export]
macro_rules! ion_struct_extend {

  ( $dest:ident ; ) => {{
  }};

  ( $dest:ident ; $key:ident : { $($val:tt)* } ) => {{
    $dest.insert(stringify!($key), ion_struct!{ $($val)* });
  }};

  ( $dest:ident ; $key:ident : [ $($val:tt)* ] ) => {{
    $dest.insert(stringify!($key), ion_list!{ $($val)* });
  }};

  ( $dest:ident ; $key:ident : $val:expr ) => {{
    $dest.insert(stringify!($key), $crate::AnionValue::from($val));
  }};

  ( $dest:ident ; $key:ident : { $($val:tt)* } , $($rest:tt)* ) => {{
    $dest.insert(stringify!($key), ion_struct!{ $($val)* });
    ion_struct_extend!($dest; $($rest)*)
  }};

  ( $dest:ident ; $key:ident : [ $($val:tt)* ] , $($rest:tt)* ) => {{
    $dest.insert(stringify!($key), ion_list!{ $($val)* });
    ion_struct_extend!($dest; $($rest)*)
  }};

  ( $dest:ident ; $key:ident : $val:expr , $($rest:tt)* ) => {{
    $dest.insert(stringify!($key), $crate::AnionValue::from($val));
    ion_struct_extend!($dest; $($rest)*)
  }};

  ( $dest:ident ; $key:ident -> $val:expr , $($rest:tt)* ) => {{
    $dest.insert(stringify!($key), $val);
    ion_struct_extend!($dest; $($rest)*)
  }};

}



macro_rules! anionvalue_partial_eq {
  ($typename:ident, $valtype:ident) => {
    impl std::cmp::PartialEq<$typename> for AnionValue {
      #[inline]
      fn eq(&self, rhs: &$typename) -> bool
      {
        match self {
          &AnionValue::$valtype(Some(ref s)) => s == rhs,
          _ => false,
        }
      }
    }
  }
}

macro_rules! anionvalue_from_type {
  ($typename:ident, $valtype:ident) => {
    impl std::convert::From<$typename> for AnionValue {
      #[inline]
      fn from(x: $typename) -> Self
      { AnionValue::$valtype(Some(x)) }
    }
  }
}

macro_rules! map_type_to_anionvalue_subtype {
  ($typename:ident, $valtype:ident) => {
      anionvalue_partial_eq!($typename, $valtype);
      anionvalue_from_type!($typename, $valtype);
  }
}



macro_rules! impl_math_selfoperators {
  ($typename:ident) => {
    use std::ops::{Add, Sub};

    impl Add for $typename {
      type Output = $typename;

      fn add(self, rhs: $typename) -> $typename {
        $typename {
          imp: self.imp + rhs.imp
        }
      }
    }

    impl Sub for $typename {
      type Output = $typename;

      fn sub(self, rhs: $typename) -> $typename {
        $typename {
          imp: self.imp - rhs.imp
        }
      }
    }

  }
}




mod test {
  #[allow(unused_imports)]
  use types::*;

  #[test]
  fn test_empty_list()
  {
    let list = ion_list![];
    assert_eq!(list, AnionList::new());
  }
  #[test]
  fn test_int_list()
  {
    let list = ion_list![1];
    assert_eq!(list, ion_list![AnionInt::from(1)]);
  }
}
