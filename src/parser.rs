//! [\file]:# (src/parser.rs)
//!
//! Module containing the functions required to take text and
//! interpret ion values
//!

// These are required to suppress false positives as
// the macros hide function usage.
#![allow(unused_imports)]
#![allow(dead_code)]

use value::{AnionValue, NonNullAnionValue};

use super::Result;

use errors::Error;

pub use self::grammar::ParseError;

// Load grammar from build.rs output
mod grammar {

  use AnionValue;
  use types::*;

  use std::char;

  include!(concat!(env!("OUT_DIR"), "/anion-grammar.peg.rs"));

  #[cfg_attr(rustfmt, rustfmt_skip)]
  #[allow(unused_attributes)]
  mod timestamp {
    use super::AnionTimestamp;
    use types::timestamp::{DateTime, TimePrecision};

    #[inline]
    fn timestamp(s: String, p: TimePrecision) -> AnionTimestamp
    {
      let dt = DateTime::parse_from_rfc3339(&s).unwrap();
      AnionTimestamp::from_datetime_with_precision(&dt, p)
    }

    #[inline] pub fn from_y(d: &str) -> AnionTimestamp { timestamp(format!("{}-01-01T00:00:00+00:00", d), TimePrecision::Year) }
    #[inline] pub fn from_ym(d: &str) -> AnionTimestamp { timestamp(format!("{}-01T00:00:00+00:00", d), TimePrecision::Month) }
    #[inline] pub fn from_ymd(d: &str) -> AnionTimestamp { timestamp(format!("{}T00:00:00+00:00", d), TimePrecision::Day) }
    #[inline] pub fn from_ymdh(d: &str, tz: &str) -> AnionTimestamp { timestamp(format!("{}:00:00{}", d, tz), TimePrecision::Hour) }
    #[inline] pub fn from_ymdhm(d: &str, tz: &str) -> AnionTimestamp { timestamp(format!("{}:00{}", d, tz), TimePrecision::Minute) }
    #[inline] pub fn from_ymdhms(d: &str, tz: &str) -> AnionTimestamp { timestamp(format!("{}{}", d, tz), TimePrecision::Second) }
    #[inline] pub fn from_ymdhmss(d: &str, tz: &str) -> AnionTimestamp { timestamp(format!("{}{}", d, tz), TimePrecision::SubSecond) }

  }

  #[inline]
  fn trim_and_unescape_string(source: &str) -> String
  {
    let (start, stop) = (1, source.len() - 1);

    let mut result: String = String::with_capacity(source.len() - 2);
    let mut chars = source[start..stop].chars();

    // function for folding a stream of hex numbers into a single
    let hex_to_char = &|r, c: char| (r << 4) + c.to_digit(16).unwrap();

    while let Some(c) = chars.next() {
      let next = if c != '\\' {
        c
      } else {
        // handle escape character
        // let first = chars.next().unwrap();
        match chars.next().unwrap() {
          '0' => '\u{00}',
          'a' => '\u{07}',
          'b' => '\u{08}',
          't' => '\u{09}', // '\t'
          'n' => '\u{0A}', // '\n'
          'v' => '\u{0B}',
          'f' => '\u{0C}',
          'r' => '\u{0D}',
          'x' => char::from_u32(chars.by_ref().take(2).fold(0, hex_to_char)).unwrap(),
          'u' => char::from_u32(chars.by_ref().take(4).fold(0, hex_to_char)).unwrap(),
          'U' => char::from_u32(chars.by_ref().take(8).fold(0, hex_to_char)).unwrap(),
          'N' => {
            assert_eq!(chars.next().unwrap(), 'L');
            continue;
          },
          ch => ch,
        }
      };
      result.push(next);
    }

    return result;
  }

  #[inline]
  fn reduce_int_string(n: &str) -> String
  {
    if n.chars().next().unwrap() == '-' {
      String::from("-") + &n[3..].replace("_", "")
    } else {
      // skip leading "0x"
      n[2..].replace("_", "")
    }
  }
}

use self::grammar::ion;
use std::char;

pub fn parse_string(source: &str) -> Option<AnionValue>
{
  ion(source).ok()
}

#[inline]
pub fn parse(source: &str) -> Result<AnionValue>
{
  ion(source).map_err(Error::ParseError)
}

#[inline]
pub fn parse_nonnull(source: &str) -> Result<NonNullAnionValue>
{
  parse(source)?.into_nonnull()
}

// Begin Test Code
//

#[allow(unused_attributes)]
#[cfg_attr(rustfmt, rustfmt_skip)]
mod test {

use std::str::FromStr;
use types::*;


macro_rules! equality_test {
    (
      $test_name:ident,
      $anion_type:path,
      $convert_expr:expr,
      $list:expr
    ) => {
        #[test]
        fn $test_name() {
            for &(src, ex) in $list.iter() {
                let value = grammar::ion(src).expect("Failed to parse ion source");
                let expected_value = $anion_type(Some($convert_expr(ex)));
                assert_eq!(value, expected_value);
            }
        }
    };

    (
      $test_name:ident,
      $anion_type:path,
      $list:expr
    ) => {
        #[test]
        fn $test_name() {
            for &(src, ex) in $list.iter() {
                let value = grammar::ion(src).expect("Failed to parse ion source");
                let expected_value = $anion_type(Some(ex.into()));
                assert_eq!(value, expected_value);
            }
        }
    }
}


equality_test!(
  integer_equality_test,
  AnionValue::Integer,
  [
    ("42", 42),
    ("0", 0),
    ("-1000", -1000),
    ("0x42", 66),
    ("3_141_592_6", 31415926),
    ("-101010101", -101010101),
    ("-0x10101", -65793),
    ("0o42", 34),
    ("0o1010_1", 4161),
    ("0b10101", 21),
    ("0b10_10", 10),
    ("0b1101_1111_0101", 3573),
  ]);


equality_test!(
  decimal_equality_test,
  AnionValue::Decimal,
  [
    ("1.0", "1.0"),
    ("0.", "0.0"),
    ("0.0", "0.0"),
    (".0", "0.0"),
    ("-.0", "0.0"),
    ("0d0", "0.0"),
    ("-0d0", "-0.0"),
    (".012", "0.012"),
    ("42.", "42.0"),
    ("0.25", "0.25"),
    ("-12.21", "-12.21"),
    ("1d-1", "0.1"),
  ]);


equality_test!(
  float_equality_test,
  AnionValue::Float,
  [
    ("1e3", 1000.0),
    ("-12.21e1", -122.1),
    ("0.432_1e3", 432.1),
    ("0.e3", 0.0),
    ("5.e2", 500.0),
    ("500_000.0000e-3", 500.0),
    (".123e-3", 0.000123),
  ]);


equality_test!(
  string_equality_test,
  AnionValue::Str,
  [
    ("\"\"", ""),
    ("\"a\"", "a"),
    ("\"$a\"", "$a"),
    (r#""0""#, "0"),
    (r#""hello""#, "hello"),
    (r#""foo\"bar""#, "foo\"bar"),
    (r#""foo\NLbar""#, "foobar"),
    (r#""\NLfoo\NLbar\NL""#, "foobar"),
    (r#""\NLfoo\\NLbar\NL""#, r"foo\NLbar"),
    (r#"":\u2764:""#, ":❤:"),
  ]);


equality_test!(
  symbol_equality_test,
  AnionValue::Symbol,
  [
    ("''", ""),
    ("a", "a"),
    ("$a", "$a"),
    ("a0_foo", "a0_foo"),
    ("null_foo", "null_foo"),
    ("'a'", "a"),
    (r"'\'a'", r"'a"),
  ]);

use super::{grammar, AnionValue};
use types::timestamp::{DateTime, TimePrecision};

equality_test!(
  timestamp_equality_test,
  AnionValue::Timestamp,
  |ex: (&str, TimePrecision)| {
    let dt = DateTime::parse_from_rfc3339(ex.0).expect(&format!("Failed to parse expected datetime str {:?}", ex.0));
    AnionTimestamp::from_datetime_with_precision(&dt, ex.1)
  },
  [
    // notime
    ("2001T", ("2001-01-01T00:00:00Z", TimePrecision::Year)),
    ("2007-05T", ("2007-05-01T00:00:00Z", TimePrecision::Month)),
    ("2007-06-29T", ("2007-06-29T00:00:00Z", TimePrecision::Day)),
    ("2008-10-22", ("2008-10-22T00:00:00Z", TimePrecision::Day)),

    // with time
    ("2007-02-23T12Z", ("2007-02-23T12:00:00Z", TimePrecision::Hour)),
    ("2007-02-23T12:14Z", ("2007-02-23T12:14:00Z", TimePrecision::Minute)),
    ("2007-02-23T20:14:33-00:00", ("2007-02-23T20:14:33.0000+00:00", TimePrecision::Second)),
    ("2007-02-23T20:14:33.079+00:00", ("2007-02-23T20:14:33.079+00:00", TimePrecision::SubSecond)),
    ("2007-02-23T20:14:33.079283742938472938472398472983472934+00:00", ("2007-02-23T20:14:33.079283742938472938472398472983472934+00:00", TimePrecision::SubSecond)),
  ]);


equality_test!(
  blob_equality_test,
  AnionValue::Blob,
  |ex: &Vec<u8>| {
    AnionBlob::from(ex.clone())
  },
  [
    ("{{   }}", &Vec::new()),
    ("{{+AB/}}", &vec![248, 0, 127]),
    ("{{aGVsbG8gZnJpZW5k}}", &vec![104, 101, 108, 108, 111, 32, 102, 114, 105, 101, 110, 100]),
    ("{{  VG8gaW5maW5pdHkuLi4gYW5kIGJleW9uZCE= }}", &vec![84, 111, 32, 105, 110, 102, 105, 110, 105, 116, 121, 46, 46, 46, 32, 97, 110, 100, 32, 98, 101, 121, 111, 110, 100, 33]),
  ]);

#[test]
fn list_equality_test()
{
  let list = vec![
    ("[]", vec![]),
    ("[42]", vec![AnionValue::from(42)]),
    ("[ 42 ]", vec![AnionValue::from(42)]),
    ("[ 42, ]", vec![AnionValue::from(42)]),
    ("[123, 21.12]", vec![AnionValue::from(123), AnionValue::from(AnionDecimal::from("21.12"))]),
    ("[null.list, []]", vec![AnionValue::List(None), AnionValue::from(AnionList::new())]),
  ];

  for &(src, ref ex) in list.iter() {
    let value: AnionValue = src.parse().unwrap();
    assert_eq!(&value, ex);
  }
}


#[test]
fn null_parsing_test()
{
  let value = grammar::ion("null").unwrap();
  let expected_value = AnionValue::Null;
  assert_eq!(value, expected_value);

  let value = grammar::ion("null.null").unwrap();
  assert_eq!(value, expected_value);
}

}
