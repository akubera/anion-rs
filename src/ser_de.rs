//! [\file]:# (src/ser_des.rs)
//!
//! Support for (de)serialization using popular serde crate.
//!
//! This module is optional, and only compiled if the 'serde' feture
//! is enabled via `anion = { version = "*", features = ["serde"] }`
//!

use serde;
use AnionValue;
