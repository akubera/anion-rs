//! [\file]:# (src/types/blob.rs)
//!
//! Binary blobs
//!

use AnionValue;

use std::{self, fmt};
use base64;


#[derive(PartialEq, Clone)]
pub struct AnionBlob {
  data: Vec<u8>,
}

impl fmt::Display for AnionBlob {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    write!(f, "{{{{ {} }}}}", base64::encode_config(&self.data, base64::STANDARD))
  }
}

impl fmt::Debug for AnionBlob {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    let iter = self.data.iter();

    f.write_str("b'")?;

    // for n in iter.take(15) {
    //   write!(f, "\\x{:x}", n)?
    // }

    for n in iter {
      write!(f, "\\x{:x}", n)?;
    }
    f.write_str("'")
  }
}

impl<'a> From<&'a str> for AnionBlob {
  fn from(source: &'a str) -> Self
  {
    let data = base64::decode_config(source, base64::STANDARD).unwrap();
    AnionBlob { data: data }
  }
}

impl From<Vec<u8>> for AnionBlob
{
  fn from(source: Vec<u8>) -> Self
  {
    AnionBlob { data: source }
  }
}

impl<'a> From<&'a [u8]> for AnionBlob
{
  fn from(source: &'a[u8]) -> Self
  {
    AnionBlob { data: source.into() }
  }
}


map_type_to_anionvalue_subtype!(AnionBlob, Blob);
