//! [\file]:# (src/types/bool.rs)
//!
//! Boolean implementation
//!

use std::{self, fmt};
use AnionValue;


#[derive(Copy, Clone, PartialEq, Debug)]
pub struct AnionBool {
  imp: bool,
}

impl AnionBool {
}

impl std::convert::From<bool> for AnionValue {
  fn from(x: bool) -> AnionValue
  {
    AnionValue::Boolean(Some(AnionBool { imp: x }))
  }
}

impl fmt::Display for AnionBool {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    write!(f, "{}", self.imp)
  }
}

map_type_to_anionvalue_subtype!(AnionBool, Boolean);
