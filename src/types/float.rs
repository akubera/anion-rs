//! [\file]:# (src/types/float.rs)
//!
//! Float/Decimal structures for anion
//!

use AnionValue;
use bigdecimal::BigDecimal;
use std::{self, fmt};
use std::str::FromStr;

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct AnionFloat {
  imp: f64,
}

macro_rules! impl_float_conversion {
  ($float_type:ident) => {
    impl From<$float_type> for AnionFloat {
      #[inline]
      fn from(float_val: $float_type) -> Self {
        AnionFloat {
          imp: float_val as f64
        }
        // AnionValue::Float(Some(float_val as f64))
      }
    }
  }
}

impl fmt::Display for AnionFloat {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    self.imp.fmt(f)
  }
}


impl_math_selfoperators!(AnionFloat);

impl_float_conversion!(f32);
impl_float_conversion!(f64);

map_type_to_anionvalue_subtype!(AnionFloat, Float);


#[derive(Clone, PartialEq, Debug)]
pub struct AnionDecimal {
  imp: BigDecimal,
}

impl From<BigDecimal> for AnionValue {
  #[inline]
  fn from(number: BigDecimal) -> Self
  {
    AnionValue::Decimal(Some(AnionDecimal { imp: number }))
  }
}

impl From<String> for AnionDecimal {
  #[inline]
  fn from(s: String) -> Self
  {
    AnionDecimal { imp: BigDecimal::from_str(&s).unwrap() }
  }
}

impl<'a> From<&'a str> for AnionDecimal {
  #[inline]
  fn from(s: &'a str) -> Self
  {
    AnionDecimal { imp: BigDecimal::from_str(s).unwrap() }
  }
}


impl fmt::Display for AnionDecimal {
  #[inline]
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    write!(f, "\"{}\"", self.imp)
  }
}


map_type_to_anionvalue_subtype!(AnionDecimal, Decimal);
