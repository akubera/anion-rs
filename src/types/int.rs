//! [\file]:# (src/types/int.rs)
//!
//! Integer
//!

use std::{self, fmt, error, result};

use AnionValue;
use num_bigint::{ParseBigIntError, BigInt};
use num_traits::Num;


#[derive(Clone, PartialEq, Debug)]
pub struct AnionInt {
  imp: BigInt,
}

impl AnionInt {
  pub fn from_str_radix(x: &str, radix: u8) -> AnionInt
  {
    AnionInt { imp: BigInt::from_str_radix(x, radix as u32).unwrap() }
  }

  pub fn parse(source: &str) -> Result
  {
    Ok(AnionInt { imp: BigInt::from_str_radix(source, 10)? })
  }

  /// Returns an integer with value 0
  pub fn zero() -> AnionInt
  {
    AnionInt { imp: BigInt::from(0) }
  }
}

impl fmt::Display for AnionInt {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    write!(f, "{}", self.imp)
  }
}

macro_rules! impl_int_conversion {
  ($int_type:ident) => {
    impl From<$int_type> for AnionValue {
      #[inline]
      fn from(int: $int_type) -> Self {
        AnionValue::Integer(Some(int.into()))
      }
    }
    impl From<$int_type> for AnionInt {
      #[inline]
      fn from(int: $int_type) -> Self {
        AnionInt { imp : BigInt::from(int) }
      }
    }
  }
}

impl_int_conversion!(i8);
impl_int_conversion!(i16);
impl_int_conversion!(i32);
impl_int_conversion!(i64);
impl_int_conversion!(u8);
impl_int_conversion!(u16);
impl_int_conversion!(u32);
impl_int_conversion!(u64);
impl_int_conversion!(BigInt);

map_type_to_anionvalue_subtype!(AnionInt, Integer);




#[derive(PartialEq, Debug)]
pub enum ParsingError {
  ParseInt(ParseBigIntError),
  LeadingZeros,
}

impl From<ParseBigIntError> for ParsingError {
  fn from(err: ParseBigIntError) -> ParsingError
  {
    ParsingError::ParseInt(err)
    // use self::ParsingError::*;
    // match err {
    //   ParseBigIntError::ParseInt(e) => ParsingError::ParseInt(e),
    //   ParseBigIntError::Other => Other,
    // }
  }
}

impl error::Error for ParsingError {
  fn description(&self) -> &str
  {
    use self::ParsingError::*;
    match *self {
      ParsingError::ParseInt(ref error) => error::Error::description(error),
      LeadingZeros => "Invalid leading zeros",
    }
  }
}

impl fmt::Display for ParsingError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    use self::ParsingError::*;
    match *self {
      ParseInt(ref err) => fmt::Display::fmt(err, f),
      LeadingZeros => f.write_str("Invalid leading zeros"),
    }
  }
}

pub type Result = result::Result<AnionInt, ParsingError>;


mod test {
  #[allow(unused_imports)]
  use super::AnionInt;

  #[test]
  fn test_int_zero()
  {
    assert_eq!(AnionInt::zero(), AnionInt::from(0));
  }

  #[test]
  fn test_parse()
  {
    assert_eq!(AnionInt::parse("123").unwrap(), AnionInt::from(123));
  }

}
