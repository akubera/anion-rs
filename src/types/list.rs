//! [\file]:# (src/types/list.rs)
//!
//! List type
//!

use std;

use AnionValue;

#[derive(Clone, PartialEq, Debug)]
pub struct AnionList {
  imp: Vec<AnionValue>,
}

impl AnionList {
  pub fn new() -> AnionList
  {
    AnionList { imp: Vec::new() }
  }

  pub fn push(&mut self, item: AnionValue)
  {
    self.imp.push(item)
  }
}


impl std::ops::Index<usize> for AnionList {
  type Output = AnionValue;

  #[inline]
  fn index(&self, index: usize) -> &AnionValue
  {
    &self.imp[index]
  }
}


impl std::convert::From<Vec<AnionValue>> for AnionList {
  fn from(v: Vec<AnionValue>) -> AnionList
  {
    AnionList { imp: v }
  }
}

impl std::cmp::PartialEq<Vec<AnionValue>> for AnionList {
  fn eq(&self, rhs: &Vec<AnionValue>) -> bool
  {
    &self.imp == rhs
  }
}

impl std::fmt::Display for AnionList {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result
  {
    f.write_str("[")?;
    let mut iter = self.imp.iter();
    if let Some(ref val) = iter.next() {
      val.fmt(f)?;
      for x in iter {
        write!(f, ", {}", x)?;
      }
    } else {
      f.write_str(" ")?;
    }
    f.write_str("]")
  }
}


map_type_to_anionvalue_subtype!(AnionList, List);
