//! [\file]: # (anion/src/types/mod.rs)
//!
//! Custom types used by the meta-type AnionValue.
//!
//! Most "Anion*" types have an equivalent standard rust type, such
//! as AnionFloat being an alias to f64, but some are too complex and
//! have origins in other crates, such unbounded integers being
//! represented by the num::BigInt struct.
//! Most of the time these should be treated as implementation details,
//! and you should just ignore the underlaying type.
//!

pub mod timestamp;

mod int;
mod float;
mod bool;
mod blob;
mod string;
mod symbol;
mod list;
mod object;


/// Simple boolean value - mapped directly to rust's *bool*
pub type AnionBool = bool::AnionBool;
// pub use self::bool::AnionBool;

/// 64-bit floating point number
pub type AnionFloat = float::AnionFloat;
// pub use self::float::AnionFloat;

/// Unbounded integer
pub type AnionInt = int::AnionInt;
// pub use self::int::AnionInt;

/// Exact decimal
pub type AnionDecimal = float::AnionDecimal;

/// Unicode characters
pub type AnionString = string::AnionString;

/// Unicode characters - may be used for keys and symbols (not values)
pub type AnionSymbol = symbol::AnionSymbol;

/// point in time
pub type AnionTimestamp = timestamp::AnionTimestamp;

/// Raw binary data
pub type AnionBlob = blob::AnionBlob;

/// List of AnionValue items
pub type AnionList = list::AnionList;

/// Map of symbols to arbitrary AnionValues
pub type AnionStruct = object::AnionStruct;


pub mod errors {
  // pub use self::int::ParsingError as IntegerError;
  pub type IntegerError = super::int::ParsingError;

}
