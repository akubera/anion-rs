//! [\file]:# (src/types/object.rs)
//!
//! Ion Struct implementation
//!

use std::{self, fmt};
use std::collections::btree_map::{BTreeMap as ImplMap,
// use std::collections::hash_map::{HashMap as ImplMap,
  Iter as ImplIter, IterMut as ImplIterMut, Keys as ImplKeyIter, Values as ImplValueIter, ValuesMut as ImplValuesMut};

use AnionValue;
use types::symbol::AnionSymbol;

// Internal type of the Struct/Map/Object type
type ImplType = ImplMap<AnionSymbol, AnionValue>;

/// Unordered collection of name/value pairs
///
/// http://amznlabs.github.io/ion-docs/spec.html#struct
///
#[derive(Clone, PartialEq, Debug)]
pub struct AnionStruct {
  imp: ImplType,
}

impl AnionStruct {
  /// Makes an empty structure
  #[inline]
  pub fn new() -> AnionStruct
  {
    AnionStruct {
      imp: ImplType::new()
    }
  }

  /// Returns the number of elements in the struct
  #[inline]
  pub fn len(&self) -> usize
  {
    self.imp.len()
  }

  /// True if empty struct '{}'
  #[inline]
  pub fn is_empty(&self) -> bool
  {
    self.imp.is_empty()
  }

  /// Returns true if there is a symbol matching 'key' stored in
  /// structure
  ///
  #[inline]
  pub fn contains_key(&self, key: &AnionSymbol) -> bool
  {
    self.imp.contains_key(key)
  }

  /// Get reference to value corresponding to the key argument
  #[inline]
  pub fn get(&self, key: &AnionSymbol) -> Option<&AnionValue>
  {
    self.imp.get(key)
  }

  /// Get mutable reference to value corresponding to the key argument
  #[inline]
  pub fn get_mut(&mut self, key: &AnionSymbol) -> Option<&mut AnionValue>
  {
    self.imp.get_mut(key)
  }


  // ================== //
  //  Mutation Methods  //
  // ================== //

  /// Inserts a key-value pair into the map.
  ///
  /// If the map did not have this key present, None is returned.
  ///
  /// If the map did have this key present, the value is updated, and
  /// the old value is returned.
  ///
  #[inline]
  pub fn insert<KeyType, ValueType>(&mut self, key: KeyType, value: ValueType) -> Option<AnionValue>
    where KeyType: Into<AnionSymbol>, ValueType: Into<AnionValue>
  {
    self.imp.insert(key.into(), value.into())
  }

  /// Remove and return object
  #[inline]
  pub fn remove(&mut self, key: &AnionSymbol) -> Option<AnionValue>
  {
    self.imp.remove(key)
  }

  /// Remove all objects
  #[inline]
  pub fn clear(&mut self)
  {
    self.imp.clear()
  }


  // ================== //
  //  Iterator Methods  //
  // ================== //

  /// Mutable iterator over (key, value) pairs
  #[inline]
  pub fn iter(&self) -> ItemIter {
    self.imp.iter()
  }

  /// Iterator over mutable (key value)
  #[inline]
  pub fn iter_mut(&mut self) -> ItemMutIter {
    self.imp.iter_mut()
  }

  /// Iterator over only keys
  #[inline]
  pub fn keys(&self) -> KeyIter {
    self.imp.keys()
  }

  /// Iterator over only values
  #[inline]
  pub fn values(&self) -> ValueIter {
    self.imp.values()
  }

  /// Iterator over mutable values
  #[inline]
  pub fn values_mut(&mut self) -> ValueMutIter {
    self.imp.values_mut()
  }
}

impl fmt::Display for AnionStruct {
  #[inline]
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    write!(f, "{{ }}")
  }
}

map_type_to_anionvalue_subtype!(AnionStruct, Struct);


// =========================== //
//  Iterator Type Definitions  //
// =========================== //

/// Iterator over key-value pairs in an object
///
/// Usually created via call to `obj.iter()`
///
/// Alternatively, calls to `obj.key()`, `obj.value()` produce
/// only keys and values.
///
type ItemIter<'a> = ImplIter<'a, AnionSymbol, AnionValue>;

/// Iterator over mutable key-value pairs
///
/// Created via `obj.iter_mut()`
///
type ItemMutIter<'a> = ImplIterMut<'a, AnionSymbol, AnionValue>;

/// Iterator over keys in object (AnionSymbols)
///
/// Created via `obj.keys()`
///
type KeyIter<'a> = ImplKeyIter<'a, AnionSymbol, AnionValue>;

/// Iterator over values
///
/// Created via `obj.values()`
///
type ValueIter<'a> = ImplValueIter<'a, AnionSymbol, AnionValue>;

/// Iterator over mutable values
///
/// Create via `obj.values_mut()`
///
type ValueMutIter<'a> = ImplValuesMut<'a, AnionSymbol, AnionValue>;



#[cfg(test)]
mod test {
  use super::AnionStruct;

  #[test]
  fn test_new_struct() {
    let s = AnionStruct::new();
    assert!(s.is_empty());
    assert_eq!(s.len(), 0);
  }

  #[test]
  fn test_contains_key() {
    let mut s = AnionStruct::new();
    s.insert("foo", "bar");
    assert_eq!(s.len(), 1);
    assert!(s.contains_key(&"foo".into()));
  }

  #[test]
  fn test_struct_from_macro() {
    let o = ion_struct!{};
    assert!(o.is_empty());
    assert_eq!(o.len(), 0);
  }

  #[test]
  fn test_struct_from_macro_1() {
    let o = ion_struct!{
      a: "abc",
    };
    assert_eq!(o.len(), 1);
    let sym = "a".into();
    assert!(o.contains_key(&sym));
    assert_eq!(o.get(&sym), Some(&"abc".into()));
  }

  #[test]
  fn test_struct_from_macro_2() {
    let o = ion_struct!{
      a: "abc", b: 123
    };
    assert_eq!(o.len(), 2);
    let sym = "a".into();
    assert!(o.contains_key(&sym));
    assert_eq!(o.get(&sym), Some(&"abc".into()));
  }

  #[test]
  fn test_struct_from_macro_substruct() {
    let o = ion_struct!{
      a: {b: "abc"}
      , y: "z",
    };
    assert!(o.get(&"y".into()).unwrap().is_str());
    assert_eq!(o.len(), 2);
  }

  #[test]
  fn test_struct_from_macro_sublist() {
    let o = ion_struct!{
      a: ["abc", 1]
    };
    assert_eq!(o.len(), 1);
    assert!(o.get(&"a".into()).unwrap().is_list());
  }

}
