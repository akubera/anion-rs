//! [\file]:# (src/types/string.rs)
//!
//! Ion string implementation
//!

use std::{self, fmt};

use AnionValue;

#[derive(Clone, PartialEq, Debug)]
pub struct AnionString {
  contents: String,
}

impl AnionString {
  pub fn with_capacity(cap: usize) -> Self
  {
    AnionString { contents: String::with_capacity(cap) }
  }
}

impl fmt::Display for AnionString {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    write!(f, "\"{}\"", self.contents)
  }
}

impl<'a> From<&'a str> for AnionString {
  fn from(s: &'a str) -> Self
  {
    AnionString { contents: s.into() }
  }
}

impl From<String> for AnionString {
  fn from(s: String) -> Self
  {
    AnionString { contents: s }
  }
}

impl<'a> From<&'a str> for AnionValue {
  fn from(s: &'a str) -> Self
  {
    AnionString::from(s).into()
  }
}


map_type_to_anionvalue_subtype!(AnionString, Str);
