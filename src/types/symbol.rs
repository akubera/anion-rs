//! [\file]:# (src/types/symbol.rs)
//!
//! Symbol type
//!

use std::{self, fmt, cmp};

use AnionValue;

#[derive(Clone, PartialEq, Debug)]
pub struct AnionSymbol {
  imp: String,
}

impl std::convert::From<String> for AnionSymbol {
  fn from(s: String) -> AnionSymbol
  {
    AnionSymbol { imp: s }
  }
}

impl<'a> std::convert::From<&'a str> for AnionSymbol {
  fn from(s: &'a str) -> AnionSymbol
  {
    AnionSymbol { imp: s.into() }
  }
}

impl fmt::Display for AnionSymbol {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    write!(f, "'{}'", self.imp)
  }
}

impl cmp::Eq for AnionSymbol {
}

impl cmp::Ord for AnionSymbol {
  fn cmp(&self, rhs: &AnionSymbol) -> cmp::Ordering
  {
    cmp::Ord::cmp(&self.imp, &rhs.imp)
  }
}
impl cmp::PartialOrd for AnionSymbol {
  fn partial_cmp(&self, rhs: &AnionSymbol) -> Option<cmp::Ordering>
  {
    cmp::PartialOrd::partial_cmp(&self.imp, &rhs.imp)
  }
}


map_type_to_anionvalue_subtype!(AnionSymbol, Symbol);
