

use std::{self, fmt};

use AnionValue;

cfg_if! {
  if #[cfg(feature = "chrono")] {
    use chrono;
    pub type ChronoDateTime = chrono::datetime::DateTime<chrono::offset::fixed::FixedOffset>;
    pub type DateTime = ChronoDateTime;
  } else {
    pub struct DateTime;
  }
}

/// Timestamps following the RFC3339 standard
///
/// [specification](http://amznlabs.github.io/ion-docs/spec.html#timestamp)
#[derive(PartialEq, Clone)]
pub struct AnionTimestamp {
  dt: DateTime,
  precision: TimePrecision,
}

#[derive(PartialEq, Clone, Copy)]
pub enum TimePrecision {
  Year,
  Month,
  Day,
  Hour,
  Minute,
  Second,
  SubSecond,
}

impl AnionTimestamp {
  pub fn from_datetime_with_precision(dt: &DateTime, p: TimePrecision) -> Self
  {
    AnionTimestamp {
      dt: dt.clone(),
      precision: p,
    }
  }
}

impl fmt::Display for AnionTimestamp {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    let format = match self.precision {
      TimePrecision::Year => "%YT",
      TimePrecision::Month => "%Y-%mT",
      TimePrecision::Day => "%Y-%m-%d",
      TimePrecision::Hour => "%Y-%m-%dT%H%:z",
      TimePrecision::Minute => "%Y-%m-%dT%R%:z",
      TimePrecision::Second | TimePrecision::SubSecond => "%+",
    };
    write!(f, "{}", self.dt.format(format))
  }
}

impl fmt::Debug for AnionTimestamp {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    write!(f, "{}", self.dt.to_rfc3339())
  }
}

cfg_if! {
  if #[cfg(feature = "chrono")] {
    impl From<ChronoDateTime> for AnionTimestamp {
      fn from(dt: ChronoDateTime) -> AnionTimestamp
      {
        AnionTimestamp {
          dt: dt,
          precision: TimePrecision::SubSecond
          ,
        }
      }
    }

    impl<'a> From<&'a ChronoDateTime> for AnionTimestamp {
      fn from(dt: &'a ChronoDateTime) -> AnionTimestamp
      {
        dt.clone().into()
      }
    }
/*
    impl From<ChronoDateTime> for AnionValue {
      fn from(dt: ChronoDateTime) -> AnionTimestamp
      {
            AnionValue::Timestamp(Some(dt.into()))
      }
    }
*/
  } else {

impl<'a> From<&'a DateTime> for AnionTimestamp {
  fn from(dt: &'a DateTime) -> AnionTimestamp
  {
    dt.clone().into()
  }
}

impl From<DateTime> for AnionTimestamp {
  fn from(dt: DateTime) -> AnionTimestamp
  {
    AnionTimestamp {
      dt: dt,
      precision: TimePrecision::SubSecond,
    }
  }
}

  }
}

map_type_to_anionvalue_subtype!(AnionTimestamp, Timestamp);

mod test {

  #[test]
  fn test_parse_timestamp()
  {

  }
}
