//! [\file]:# (src/value.rs)
//!
//! Module containing AnionValue - the generic anion type
//!

use std::{self, fmt};
use std::str::FromStr;
use types::*;
use parser;
use super::{Result, Error};

/// Enum of all possible types of elements in an ion document.
///
/// These are mapped to either rust literal types or other anion types.
/// All values are Options, with a None value corresponding to the
/// equivalent 'null' value of the ion document.
///
#[derive(Debug, PartialEq, Clone)]
pub enum AnionValue {
  /// Pure null type
  Null,

  /// true, false, null.bool
  Boolean(Option<AnionBool>),

  /// Bigint values (unlimited)
  Integer(Option<AnionInt>),

  /// 64 bit floating point value
  Float(Option<AnionFloat>),

  /// Exact precision real number value
  Decimal(Option<AnionDecimal>),

  /// Timestamp object
  Timestamp(Option<AnionTimestamp>),

  /// String of utf8 characters
  Str(Option<AnionString>),

  /// String of unicode characters - can be used as struct keys
  /// and in s-expressions
  Symbol(Option<AnionSymbol>),

  /// Raw binary data (base64 encoded)
  Blob(Option<AnionBlob>),

  /// List of AnionValues
  List(Option<AnionList>),

  /// Map of symbols to Arbitrary AnionValues
  Struct(Option<AnionStruct>),
}


macro_rules! impl_member_is_type {

  ($name:ident, $($t:pat)|*) => {
    pub fn $name(&self) -> bool {
      use AnionValue::*;
      match *self {
        $($t)|* => true,
        _ => false,
      }
    }
  };

  [ $( ($name:ident, $($t:pat)|* ) ),* ] => {
    $( impl_member_is_type!($name, $($t)|*); )*
  };

}

impl AnionValue {
  pub fn into_nonnull(self) -> Result<NonNullAnionValue>
  {
    use AnionValue::*;
    use NonNullAnionValue as NotNull;

    let notnull_val = match self {
      Null => NotNull::Null,
      Boolean(Some(x)) => NotNull::Boolean(x),
      Integer(Some(x)) => NotNull::Integer(x),
      Float(Some(x)) => NotNull::Float(x),
      Decimal(Some(x)) => NotNull::Decimal(x),
      Timestamp(Some(x)) => NotNull::Timestamp(x),
      Str(Some(x)) => NotNull::Str(x),
      Symbol(Some(x)) => NotNull::Symbol(x),
      Blob(Some(x)) => NotNull::Blob(x),
      _ => return Err(Error::UnexpectedNull),
    };
    Ok(notnull_val)
  }

  impl_member_is_type![
    (is_null, Null),
    (is_bool, Boolean(_)),
    (is_int, Integer(_)),
    (is_float, Float(_)),
    (is_decimal, Decimal(_)),
    (is_timestamp, Timestamp(_)),
    (is_str, Str(_)),
    (is_symbol, Symbol(_)),
    (is_blob, Blob(_)),
    (is_list, List(_)),
    (is_struct, Struct(_)),

    (is_numeric, Integer(_) | Float(_) | Decimal(_)),
    (is_text, Symbol(_) | Str(_)),
    (is_lob, Blob(_)),
    (is_container, List(_) | Struct(_))
  ];

}


impl fmt::Display for AnionValue {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    use AnionValue::*;

    match *self {
      Null => f.write_str("null.null"),
      Boolean(Some(ref x)) => fmt::Display::fmt(x, f),
      Boolean(None) => f.write_str("null.bool"),
      Integer(Some(ref x)) => fmt::Display::fmt(x, f),
      Integer(None) => f.write_str("null.int"),
      Float(Some(ref x)) => fmt::Display::fmt(x, f),
      Float(None) => f.write_str("null.float"),
      Decimal(Some(ref x)) => fmt::Display::fmt(x, f),
      Decimal(None) => f.write_str("null.decimal"),
      Timestamp(Some(ref x)) => fmt::Display::fmt(x, f),
      Timestamp(None) => f.write_str("null.timestamp"),
      Str(Some(ref x)) => fmt::Display::fmt(x, f),
      Str(None) => f.write_str("null.string"),
      Symbol(Some(ref x)) => fmt::Display::fmt(x, f),
      Symbol(None) => f.write_str("null.symbol"),
      Blob(Some(ref x)) => fmt::Display::fmt(x, f),
      Blob(None) => f.write_str("null.blob"),
      List(Some(ref x)) => fmt::Display::fmt(x, f),
      List(None) => f.write_str("null.list"),
      Struct(Some(ref x)) => fmt::Display::fmt(x, f),
      Struct(None) => f.write_str("null.struct"),
    }
  }
}


impl FromStr for AnionValue {
  type Err = super::Error;

  #[inline]
  fn from_str(s: &str) -> Result<Self>
  {
    parser::parse(s)
  }
}

impl std::cmp::PartialEq<Vec<AnionValue>> for AnionValue {
  fn eq(&self, rhs: &Vec<AnionValue>) -> bool
  {
    match self {
      &AnionValue::List(Some(ref list)) => list == rhs,
      _ => false,
    }
  }
}


/// Variant of AnionValue enum that does not permit null values.
/// This includes the 'pure NULL' null value, though this may
/// be removed due to naming sillyness. This is much closer in type
/// to true JSON values.
///
#[derive(Debug, PartialEq, Clone)]
pub enum NonNullAnionValue {
  /// The NULL value - in the NonNullValue!
  Null,

  /// true, false
  Boolean(AnionBool),

  /// Bigint values (unlimited)
  Integer(AnionInt),

  /// 64 bit floating point value
  Float(AnionFloat),

  /// Exact precision real number value
  Decimal(AnionDecimal),

  /// Timestamp object
  Timestamp(AnionTimestamp),

  /// String of unicode characters
  Str(AnionString),

  /// String of unicode characters - may be used for keys
  Symbol(AnionSymbol),

  /// Raw binary data (base64 encoded)
  Blob(AnionBlob),

  /// List of AnionValues
  List(AnionList),

  /// Map of symbols to Arbitrary AnionValues
  Struct(AnionStruct),
}

impl FromStr for NonNullAnionValue {
  type Err = super::Error;

  #[inline]
  fn from_str(s: &str) -> Result<Self>
  {
    parser::parse_nonnull(s)
  }
}
